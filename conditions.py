# -*- coding: utf-8 -*-
import RPi.GPIO as GPIO
from touch_sensor import *
from analogread import *


class Conditions(object):
    def __init__(
            self,
            SPICLK, SPIMOSI, SPIMISO, SPICS,
            POTENTIOMETER_ADC, POTENTIOMETER_STEPS,
            COLOR_CHOOSER_ADC, COLOR_CHOOSER_STEPS,
            WHITE_LEVEL_ADC, WHITE_LEVEL_STEPS,
            TOUCH_SENSOR_GPIO=None,
    ):

        # INIT TOUCH SENSOR
        if TOUCH_SENSOR_GPIO is not None:
            self.touch_sensor = TouchSensor(TOUCH_SENSOR_GPIO)
        else:
            self.touch_sensor = None

        # INIT POTENTIOMETER
        self.potentiometer_reader = AnalogReadByStep(
            SPICLK, SPIMOSI, SPIMISO, SPICS, POTENTIOMETER_ADC, POTENTIOMETER_STEPS
        )
        self.potentiometer_reader.set_refresh_interval(0.05)

        # INIT COLOR CHOOSER
        self.color_chooser = AnalogReadByStep(
            SPICLK, SPIMOSI, SPIMISO, SPICS, COLOR_CHOOSER_ADC, COLOR_CHOOSER_STEPS
        )
        self.color_chooser.set_refresh_interval(0.05)

        # INIT WHITE LEVEL
        self.white_level = AnalogReadByStep(
            SPICLK, SPIMOSI, SPIMISO, SPICS, WHITE_LEVEL_ADC, WHITE_LEVEL_STEPS
        )
        self.white_level.set_refresh_interval(0.05)

    def if_button_pressed(self, callback):
        if not self.touch_sensor:
            return
        if self.touch_sensor.is_changed():
            print("touch changed : %d" % self.touch_sensor.current_value)
            if self.touch_sensor.current_value is 1:
                callback()

    def if_trim_change(self, callback):
        if self.potentiometer_reader.is_changed():
            print("potentiometer changed : %d" % self.potentiometer_reader.current_step)
            callback(self.potentiometer_reader.current_step)

    def if_color_chooser_change(self, callback):
        if self.color_chooser.is_changed():
            print("color chooser changed : %d" % self.color_chooser.current_step)
            callback(self.color_chooser.current_step)

    def if_white_level_change(self, callback):
        if self.white_level.is_changed():
            print("white level changed : %d" % self.white_level.current_step)
            callback(self.white_level.current_step)

    def clean(self):
        self.potentiometer_reader.clean()
