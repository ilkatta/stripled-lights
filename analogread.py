# -*- coding: utf-8 -*-
import RPi.GPIO as GPIO
import time
import logging


class MCP3008(object):
    MAX_OUT_VALUE = 1024

    def __init__(self, clockpin, mosipin, misopin, cspin):
        assert type(clockpin) is int and clockpin >= 0
        assert type(mosipin) is int and mosipin >= 0
        assert type(misopin) is int and misopin >= 0
        assert type(cspin) is int and cspin >= 0
        logging.debug("MCP3008.__init__")
        self.clockpin = clockpin
        self.mosipin = mosipin
        self.misopin = misopin
        self.cspin = cspin

        GPIO.setup(self.cspin, GPIO.OUT)
        GPIO.setup(self.misopin, GPIO.IN)
        GPIO.setup(self.clockpin, GPIO.OUT)
        GPIO.setup(self.mosipin, GPIO.OUT)

    def readadc(self, adcnum):
        assert not ((adcnum > 7) or (adcnum < 0))
        if ((adcnum > 7) or (adcnum < 0)):
            return -1
        GPIO.output(self.cspin, True)

        GPIO.output(self.clockpin, False)  # start clock low
        GPIO.output(self.cspin, False)  # bring CS low

        commandout = adcnum
        commandout |= 0x18  # start bit + single-ended bit
        commandout <<= 3  # we only need to send 5 bits here
        for i in range(5):
            if (commandout & 0x80):
                GPIO.output(self.mosipin, True)
            else:
                GPIO.output(self.mosipin, False)
            commandout <<= 1
            GPIO.output(self.clockpin, True)
            GPIO.output(self.clockpin, False)

        adcout = 0
        # read in one empty bit, one null bit and 10 ADC bits
        for i in range(12):
            GPIO.output(self.clockpin, True)
            GPIO.output(self.clockpin, False)
            adcout <<= 1
            if (GPIO.input(self.misopin)):
                adcout |= 0x1

        GPIO.output(self.cspin, True)

        adcout >>= 1  # first bit is 'null' so drop it

        # if adcout > 1024 or adcout <= 0:
        # if adcout > 1024 or adcout < 0: raise Exception("readed value not valid %d" % adcout)
        assert adcout <= 1024 and adcout >= 0, "read value not valid %d" % adcout
        return adcout

    def clean(self):
        logging.debug("MCP3008.clean")
        GPIO.cleanup(self.cspin)
        GPIO.cleanup(self.misopin)
        GPIO.cleanup(self.clockpin)
        GPIO.cleanup(self.mosipin)


class AnalogReader(MCP3008):
    def __init__(self, clockpin, mosipin, misopin, cspin, adcnum):
        logging.debug("AnalogReader.__init__")
        super(AnalogReader, self).__init__(clockpin, mosipin, misopin, cspin)
        assert type(adcnum) is int and adcnum >= 0
        self.adcnum = adcnum
        self.last_value = 0
        self.tollerance = 0
        self.sync_interval = 0.1
        self.last_sync = 0
        self._current_value = 0

    def set_tollerance(self, value):
        self.tollerance = value

    def is_changed(self):
        over_tolerance = self.tollerance < abs(self.last_value - self.current_value)
        if over_tolerance:
            self.last_value = self.current_value
            return True
        return False

    @property
    def current_value(self):
        delta_sync = 0
        if self.sync_interval:
            delta_sync = self._current_time - self.last_sync

        if not self.sync_interval or self.sync_interval < delta_sync:
            self._current_value = self.readadc(self.adcnum)

        return self._current_value

    @property
    def _current_time(self):
        return time.time()


class AnalogReadByStep(AnalogReader):
    def __init__(self, clockpin, mosipin, misopin, cspin, adcnum, steps):
        assert type(steps) is int and steps > 0
        logging.debug("AnalogReadByStep.__init__")
        super(AnalogReadByStep, self).__init__(clockpin, mosipin, misopin, cspin, adcnum)
        self.steps = steps
        self.step = self.MAX_OUT_VALUE / self.steps
        self.set_tollerance(self.step / 2)
        self.last_step = 0
        self.last_change_level = 0

    def is_changed(self):
        over_tolerance = self.tollerance < abs(self.last_change_level - self.current_value)
        step_changed = self.current_step != self.last_step
        if over_tolerance and step_changed:
            self.last_change_level = self.current_value
            self.last_step = self.current_step
            return True
        return False

    @property
    def current_step(self):
        return int(self.current_value / self.step)

    def set_refresh_interval(self, seconds):
        self.sync_interval = seconds


import threading


class ReaderWorker(threading.Thread):
    def __init__(self, callback, group=None, target=None, name=None):
        logging.debug("ReaderWorker.__init__")
        threading.Thread.__init__(self, group=group, target=target, name=name, verbose=None)
        self.callback = callback
        self.stop_event = threading.Event()
        self.condition = threading.Condition()

    def is_changed(self):
        raise NotImplementedError()

    def clean(self):
        raise NotImplementedError()

    def run(self):
        while not self.is_stopped:
            try:
                if (self.is_changed()):
                    if self.condition._is_owned():
                        self.condition.notifyAll()
                    self.call_callback()

            except AssertionError as e:
                logging.warning("AssertionError: %s" % e.message)

    def stop(self):
        self.stop_event.set()
        self.join()
        self.clean()

    @property
    def is_stopped(self):
        return self.stop_event.is_set()
        # return self.stop_event.wait(delay)

    @property
    def current_value(self):
        raise NotImplementedError()

    def call_callback(self):
        self.callback(self.current_value)

    def wait_event(self):
        return self.condition.wait()


class AnalogReadByStepWorker(AnalogReadByStep, ReaderWorker):
    def __init__(self, clockpin, mosipin, misopin, cspin, adcnum, steps, callback, lock, name=None):
        logging.debug("AnalogReadByStepWorker.__init__")
        AnalogReadByStep.__init__(self, clockpin, mosipin, misopin, cspin, adcnum, steps)
        if name is None:
            name = type(self).__name__
        ReaderWorker.__init__(self, callback, name=name)
        self.lock = lock

    def call_callback(self):
        self.callback(self.current_step)

    @property
    def current_value(self):
        with self.lock:
            return super(AnalogReadByStepWorker, self).current_value
