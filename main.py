#!/usr/bin/python
# -*- coding: utf-8 -*-
# TODO: se cambi di stato troppo frequenti > disabilitare sensore
# TODO: trimmer per livello di bianco

from conditions import *
from actions import *


USE_THREAD = False

# TOUCH SENSOR SETTINGS
TOUCH_SENSOR_GPIO = 27

# SPI INTERFACE SETTINGS
SPICLK = 18
SPIMISO = 23
SPIMOSI = 24
SPICS = 25

# POTENTIOMETER SETTINGS
POTENTIOMETER_ADC = 0
POTENTIOMETER_STEPS = 255

# COLOR CHOOSER SETTINGS
COLOR_CHOOSER_ADC = 3
COLOR_CHOOSER_STEPS = 255

# WHITE LEVEL SETTINGS
WHITE_LEVEL_ADC = 4
WHITE_LEVEL_STEPS = 255

execute = Executor(30, "/dev/spidev0.0")
execute.color_chooser_max_value(COLOR_CHOOSER_STEPS)

GPIO.setmode(GPIO.BCM)

if USE_THREAD:
    from utils4threads import *

    try:
        logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s (%(threadName)-2s) %(message)s',
                    )

        lock = threading.Lock()

        # potentiometer
        AnalogReadByStepWorker(SPICLK, SPIMOSI, SPIMISO, SPICS, POTENTIOMETER_ADC, POTENTIOMETER_STEPS,
                               callback=execute.change_brightness, lock=lock, name="potentiometer").start()
        # color chooser
        AnalogReadByStepWorker(SPICLK, SPIMOSI, SPIMISO, SPICS, COLOR_CHOOSER_ADC, COLOR_CHOOSER_STEPS, lock=lock,
                               callback=execute.change_color, name="color_chooser").start()

        # white level
        # AnalogReadByStepWorker(SPICLK, SPIMOSI, SPIMISO, SPICS, WHITE_LEVEL_ADC, WHITE_LEVEL_STEPS, callback=execute.change_white_level).start()

        TouchSensorWorker(TOUCH_SENSOR_GPIO, on_touch_event, lock=lock, name="touch_sensor").start()

        while is_anyone_alive():
            time.sleep(1)
    except KeyboardInterrupt:
        pass


    finally:
        try:
            terminate_threads()
        except NameError:
            pass
else:

    conditions = Conditions(
        SPICLK, SPIMOSI, SPIMISO, SPICS,
        POTENTIOMETER_ADC, POTENTIOMETER_STEPS,
        COLOR_CHOOSER_ADC, COLOR_CHOOSER_STEPS,
        WHITE_LEVEL_ADC, WHITE_LEVEL_STEPS,
        TOUCH_SENSOR_GPIO
    )
    try:
        while True:
            try:
                conditions.if_button_pressed(execute.toggle_on_off)

                conditions.if_trim_change(execute.change_brightness)

                conditions.if_color_chooser_change(execute.change_color)

                # conditions.if_white_level_change(execute.change_white_level)

            except AssertionError as ae:
                print(ae.message)

    except KeyboardInterrupt:
        pass

    except Exception as e:
        if conditions:
            conditions.clean()
        raise e

    finally:
        if conditions:
            conditions.clean()
