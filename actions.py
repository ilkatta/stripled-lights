# -*- coding: utf-8 -*-
from bibliopixel.led import *
from bibliopixel.drivers.LPD8806 import DriverLPD8806
from bibliopixel.drivers.spi_driver_base import ChannelOrder
import bibliopixel.colors as colors
import logging

class ColorGenerator(object):
    def __init__(self, max_color_id=255):
        assert max_color_id in [255, 360, 384]
        self._max_color_id = max_color_id

    def generate(self, brightness=255, color_id=None, white_level=None):
        '''
        :param brightness: int
        :param color_id: int
        :return: Tuple[int,int,int] rgb color
        '''
        if not color_id:
            color = colors.Snow
        elif white_level:
            color = self.generate_gradient(color_id, white_level)
        else:
            color = self.generate_gradient(color_id)

        return colors.color_scale(color, brightness)

    def generate_gradient(self, value, white_level=0):
        '''
        :param value: hue color
        :return: tuple RGB
        '''
        # return colors.int2rgb(value)

        base = [white_level, white_level, white_level]
        return colors.color_blend(self.int2rgb(value), base)

    def int2rgb(self, value):
        '''
        :param value: int
        :return: Tuple[int,int,int] rgb color
        '''
        ''' hue ( 0 - 255 ) '''
        if self._max_color_id <= 255:
            # return colors.hue2rgb(value)
            # return colors.hue2rgb_raw(value)
            # return colors.hue2rgb_spectrum(value)
            return colors.hue2rgb_rainbow(value)
        ''' ( 0 - 359 ) '''
        if self._max_color_id <= 359:
            return colors.hue2rgb_360(value)
        ''' Adafruit color position ( 0- 384 ) '''
        if self._max_color_id <= 384:
            return colors.wheel_color(value)


def update_led(funct):
    ''' function decorator '''

    def updater(*args, **kwargs):
        executor = funct(*args, **kwargs)
        if executor.is_on():
            color = executor.generate_color()
            executor.leds.fill(color)
        else:
            executor.leds.all_off()
        executor.leds.update()

    return updater


class Executor(object):
    def __init__(self, leds_number, spi_dev="/dev/spidev0.0"):
        self.brightness = 255
        self.color_id = 255
        self.white_level = 0
        self.on = True
        self._color_chooser_max = 255
        self._brightness_max = 255
        self._white_level_max = 255
        self.color_generator = ColorGenerator(self._color_chooser_max)
        self.init_leds(leds_number, spi_dev)

    def init_leds(self, num, spi_dev="/dev/spidev0.0"):
        self.driver = DriverLPD8806(num, ChannelOrder.RGB, True, spi_dev, 2)
        self.leds = LEDStrip(self.driver)
        try:
            self.leds.fillRGB(255, 0, 0)
            self.leds.update()
            time.sleep(0.5)
            self.leds.fillRGB(0, 255, 0)
            self.leds.update()
            time.sleep(0.5)
            self.leds.fillRGB(0, 0, 255)
            self.leds.update()
            time.sleep(0.5)
        except KeyboardInterrupt:
            logging.debug("switch off")
        finally:
            self.leds.all_off()
            self.leds.update()

    def color_chooser_max_value(self, value=None):
        assert type(value) is int or value is None
        if value is not None:
            self._color_chooser_max = value
            self.color_generator = ColorGenerator(self._color_chooser_max)
        return self._color_chooser_max

    def brightness_max_value(self, value=None):
        assert type(value) is int or value is None
        if value is not None:
            self._brightness_max = value
        return self._brightness_max

    def white_level_max_value(self, value=None):
        assert type(value) is int or value is None
        if value is not None:
            self._white_level_max = value
        return self._white_level_max

    @update_led
    def toggle_on_off(self):
        self.on = not self.on

        return self

    @update_led
    def change_brightness(self, value):
        assert value >= 0 and value <= self._brightness_max, "brightness value not in valid range ( %d )" % value

        self.brightness = value
        return self

    @update_led
    def change_color(self, value):
        assert value >= 0 and value <= self._color_chooser_max, "color value not in valid range ( %d )" % value

        self.color_id = value
        return self

    @update_led
    def change_white_level(self, value):
        assert value >= 0 and value <= self._white_level_max, "white level value not in valid range ( %d )" % value

        self.white_level = value
        return self

    def generate_color(self):
        return self.color_generator.generate(
            brightness=self.brightness,
            color_id=self.color_id,
            white_level=self.white_level
        )

    def is_on(self):
        return bool(self.on)
