# -*- coding: utf-8 -*-
import RPi.GPIO as GPIO
import threading
import logging
import time


class TouchSensor(object):
    _time_touch_end = 0
    _time_touch_start = 0
    last_value = 0

    def __init__(self, gpio_port):
        logging.debug("TouchSensor.__init__")
        self.gpio_port = gpio_port
        GPIO.setup(self.gpio_port, GPIO.IN)

    @property
    def current_value(self):
        if not self._time_touch_end:
            return -1
        return self._time_touch_end - self._time_touch_start

    @property
    def _current_value(self):
        return GPIO.input(self.gpio_port)

    def is_changed(self):
        if self.last_value == self._current_value:
            return False

        self.last_value = self._current_value
        if self.last_value == 1:
            self._time_touch_start = time.time()
            self._time_touch_end = 0
        else:
            self._time_touch_end = time.time()
        return self.last_value == 0

    def clean(self):
        logging.debug("TouchSensor.clean")
        GPIO.cleanup(self.gpio_port)


class TouchSensorWorker(TouchSensor, threading.Thread):
    def __init__(self, gpio_port, callback, lock, name=None):
        logging.debug("TouchSensorWorker.__init__")
        TouchSensor.__init__(self, gpio_port)
        threading.Thread.__init__(self, name=name)

        self.callback = callback
        self.stop_event = threading.Event()
        self.timer = None
        self._clicks = []

        self.condition = threading.Condition()

        self.lock = lock

    def run(self):
        while not self.is_stopped:
            if (self.is_changed()):
                self.click_event()

    def click_event(self):
        if not self.timer or not self.timer.isAlive():
            self.reset_clicks()
            self.timer = threading.Timer(0.5, self.call_callback)
            self.timer.start()

        self.click()

    def call_callback(self):
        if self.condition._is_owned():
            self.condition.notifyAll()
        self.callback(self._clicks)

    def reset_clicks(self):
        self._clicks = []

    def click(self):
        self._clicks.append(self.current_value)

    def stop(self):
        self.stop_event.set()
        self.join(1)
        self.clean()

    @property
    def is_stopped(self):
        return self.stop_event.is_set()

    def wait_event(self):
        return self.condition.wait()

    @property
    def current_value(self):
        with self.lock:
            return super(TouchSensorWorker, self).current_value
