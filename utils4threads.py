# -*- coding: utf-8 -*-
import threading
import logging

def hasmethod(obj, method):
    return callable(getattr(obj, method, None))


def terminate_threads():
    print("terminating threads")
    main_thread = threading.currentThread()

    for t in threading.enumerate():
        if t is main_thread:
            continue
        if not hasmethod(t, 'stop'):
            continue
        print("stopping '%s' thread" % t.getName())
        try:
            t.stop()
        except Exception:
            pass


def is_anyone_alive():
    main_thread = threading.currentThread()

    for t in threading.enumerate():
        if t is main_thread:
            continue
        if t.isAlive():
            return True

    return False


def on_touch_event(clicks):
    global execute
    if not clicks or not len(clicks):
        return
    click_count = len(clicks)
    first_click_time = clicks[0]
    logging.debug("n clicks: %d", click_count)
    logging.debug("click times: %s", str(clicks))

    if click_count == 1:
        pass  # single click action

    if click_count >= 2:
        pass  # double click action

    if first_click_time > 0.5:
        pass  # long tap action

    if first_click_time > 1.5:
        pass  # very long tap action

    execute.toggle_on_off()